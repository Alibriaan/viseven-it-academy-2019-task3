let count = sessionStorage.length + 1;
let currentImage;

// Лайки и дизлайки на каждой перезагрузке 
for (let i = 1; i <= sessionStorage.length; i++) {
    sessionStorage.setItem(i, JSON.stringify(Object.assign({}, JSON.parse(sessionStorage.getItem(i)), {
        likeStatus: true,
        dislikeStatus: true
    })));
}

let like = function (event) {
    let elemData = JSON.parse(sessionStorage.getItem(currentImage));
    if (elemData.likeStatus) {

        sessionStorage.setItem(currentImage, JSON.stringify(Object.assign({}, elemData, {
            like: elemData.like + 1,
            likeStatus: false
        })));

        document.querySelectorAll('.like-block p')[currentImage - 1].innerText = elemData.like + 1;
        document.querySelector('.like-current-block p').innerText = elemData.like + 1;
        document.querySelector('.like-current-block').classList.add('check');
    }
}

let dislike = function (event) {
    let elemData = JSON.parse(sessionStorage.getItem(currentImage));

    if (elemData.dislikeStatus) {
        sessionStorage.setItem(currentImage, JSON.stringify(Object.assign({}, elemData, {
            dislike: elemData.dislike + 1,
            dislikeStatus: false
        })));

        document.querySelectorAll('.dislike-block p')[currentImage - 1].innerText = elemData.dislike + 1;
        document.querySelector('.dislike-current-block p').innerText = elemData.dislike + 1;
        document.querySelector('.dislike-current-block').classList.add('check');
    }
}

let comments = function (event) {
    let date, comm, name;
    date = new Date().toLocaleDateString('en-GB', {
        hour12: true,
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric'
    });
    name = document.forms[0][1].value;
    comm = document.forms[0][2].value;
    event.preventDefault();

    if (!name || !comm || !/\S/.test(name) || !/\S/.test(comm)) {
        return;
    } else {
        let elem = JSON.parse(sessionStorage.getItem(currentImage));
        let comArray = elem.comments;
        comArray.push({
            userName: name,
            comments: comm,
            data: String(date)
        });
        sessionStorage.setItem(currentImage, JSON.stringify(Object.assign({}, elem, {
            comments: comArray
        })));

        block.querySelector('.all-comments').insertAdjacentHTML('beforeend', `<div><div class="comment-presents"><h5>By ${name}</h5> <h5>${date}</h5></div><div class="comment-information"><h4>${comm}</h4></div></div>`);

        document.forms[0][1].value = '';
        document.forms[0][2].value = '';
        document.querySelector('.current-img-block header h1').innerText = `Comments: ${elem.comments.length}`;
        document.querySelectorAll('.comments-block p')[currentImage - 1].innerHTML = elem.comments.length;
    }
}

let exit = function (event) {
    document.querySelector('.opacity-block').remove();
    document.getElementsByClassName('current-img-block')[0].remove();
    document.querySelector('main').removeAttribute('style');

    block.querySelector('.img-block img').remove();
    block.querySelector('.like-current-block p').innerText = '';
    block.querySelector('.dislike-current-block p').innerText = '';
    block.querySelector('.all-comments').innerHTML = '';
}

// основной блок
let block = document.createElement('div');
block.classList.add('current-img-block');

//сюда картинку
block.insertAdjacentHTML('beforeend', `<div class="img-block"></div>`);

// лайки дизлайки 
let reactionblock = document.createElement('div');
reactionblock.classList.add('current-reaction-block')
reactionblock.insertAdjacentHTML('beforeend', `<div class="like-current-block block"> <i class="fas fa-thumbs-up  fa-2x"></i> <p></p></div>`);
reactionblock.insertAdjacentHTML('beforeend', `<div class="dislike-current-block block"> <i class="fas fa-thumbs-down  fa-2x"></i> <p></p></div>`)

// комменты
let commentsblock = document.createElement('div');
commentsblock.classList.add('comments-block');

commentsblock.insertAdjacentHTML('beforeend', `<header>
<div><button><i class="fas fa-times fa-2x""></i>
</button></div><h1></h1></header><div class="all-comments"></div><form name="comments"><fieldset>
<input name="nick" placeholder="Hello" type=text"/>
<div><textarea name="text" placeholder="Write your comment here..."></textarea>
<button type=submit><i class="fas fa-paper-plane  fa-2x"></i>
</button></div>
</fieldset></form`);

block.append(commentsblock);
block.querySelector('.img-block').append(reactionblock);

let opacityBlock = document.createElement('div');
opacityBlock.classList.add('opacity-block');

//Евенты
block.querySelector('.like-current-block').addEventListener('click', like, false);
block.querySelector('.dislike-current-block').addEventListener('click', dislike, false);
block.querySelector('.comments-block header button').addEventListener('click', exit, false);
block.addEventListener('click', function (event) {
    event.stopImmediatePropagation();
}, false);
block.querySelector('form').addEventListener('submit', comments);

// Загрузка картинки
document.querySelector('.add-img').onchange = function (e) {

    let block = document.createElement('div');
    block.classList = "main-img-block";

    let reactionblock = document.createElement('div');
    reactionblock.classList.add('reaction-block')

    let img = new Image();
    let reader = new FileReader();

    reader.onloadend = function () {
        img.src = reader.result;

        if (img.width / img.height >= 1.5) {
            block.classList.add('width');
        } else if (img.height / img.width >= 1.2) {

            block.classList.add('height');
        } else {
            block.classList.add('none');
        }

        sessionStorage.setItem(sessionStorage.length + 1, JSON.stringify({
            image: reader.result,
            like: 0,
            dislike: 0,
            comments: 0,
            likeStatus: true,
            dislikeStatus: true,
            comments: []
        }));

        reactionblock.insertAdjacentHTML('beforeend', `<div class="like-block block"> <i class="fas fa-thumbs-up  fa-2x"></i> <p>${JSON.parse(sessionStorage.getItem(sessionStorage.length)).like}</p></div>`);
        reactionblock.insertAdjacentHTML('beforeend', `<div class="dislike-block block"> <i class="fas fa-thumbs-down  fa-2x"></i> <p>${JSON.parse(sessionStorage.getItem(sessionStorage.length)).dislike}</p></div>`);
        reactionblock.insertAdjacentHTML('beforeend', `<div class="comments-block block"> <i class="fas fa-comments  fa-2x"></i> <p>${JSON.parse(sessionStorage.getItem(sessionStorage.length)).comments.length}</p></div>`);
        block.append(img);
        block.append(reactionblock);
        block.dataset.count = count++;
        eventOnBlock(block);
        document.getElementsByTagName('main')[0].insertBefore(block, document.querySelector('.add'));
        document.querySelector("[type=file]").value = "";
    }

    if (e.target.files && (/.jpg/.test(e.target.files[0].name) || /.png/.test(e.target.files[0].name))) {
        reader.readAsDataURL(e.target.files[0]);
    } else {
        console.log("Error");
    }
}

// Загрузка картинок с сесионного хранилища
document.addEventListener('DOMContentLoaded', function () {

    for (let i = 1; i <= sessionStorage.length; i++) {
        let block = document.createElement('div');
        block.classList = "main-img-block";
        let reactionblock = document.createElement('div');
        reactionblock.classList.add('reaction-block')
        reactionblock.insertAdjacentHTML('beforeend', `<div class="like-block block"> <i class="fas fa-thumbs-up  fa-2x"></i> <p>${JSON.parse(sessionStorage.getItem(i)).like}</p></div>`);
        reactionblock.insertAdjacentHTML('beforeend', `<div class="dislike-block block"> <i class="fas fa-thumbs-down  fa-2x"></i> <p>${JSON.parse(sessionStorage.getItem(i)).dislike}</p></div>`);
        reactionblock.insertAdjacentHTML('beforeend', `<div class="comments-block block"> <i class="fas fa-comments  fa-2x"></i> <p>${JSON.parse(sessionStorage.getItem(i)).comments.length}</p></div>`);

        let image = new Image();

        image.src = JSON.parse(sessionStorage.getItem(i)).image;

        if (image.width / image.height >= 1.5) {
            block.classList.add('width');
        } else if (image.height / image.width >= 1.2) {

            block.classList.add('height');
        } else {
            block.classList.add('none');
        }

        block.dataset.count = i;
        block.append(image);
        block.append(reactionblock);
        eventOnBlock(block);
        document.getElementsByTagName('main')[0].insertBefore(block, document.querySelector('.add'))
    }
}, false);

// Добавление блоку евента 
function eventOnBlock(elem) {
    elem.addEventListener('click', function (event) {

        event.path.find(function (element, index) {
            if (element.classList && element.classList.contains('main-img-block')) {

                if (document.getElementsByClassName('current-img-block').length) {
                    document.getElementsByClassName('current-img-block')[0].remove();
                }

                currentImage = element.dataset.count;

                if (block.querySelector('.img-block img')) {
                    block.querySelector('.img-block img').remove();
                    block.querySelector('.like-current-block p').innerText = '';
                    block.querySelector('.dislike-current-block p').innerText = '';
                    block.querySelector('.all-comments').innerHTML = '';
                }

                let item = JSON.parse(sessionStorage.getItem(element.dataset.count));
                let img = new Image();
                img.src = item.image;

                block.querySelector('.img-block').prepend(img);
                block.querySelector('.like-current-block p').innerText = item.like;
                block.querySelector('.dislike-current-block p').innerText = item.dislike;
                block.querySelector('header h1').innerText = `Comments: ${item.comments.length}`;

                item.comments.forEach(function (element, index) {
                    block.querySelector('.all-comments').insertAdjacentHTML('beforeend', `<div><div class="comment-presents"><h5>By ${element.userName}</h5> <h5>${element.data}</h5></div><div class="comment-information"><h4>${element.comments}</h4></div></div>`);
                });

                document.body.append(opacityBlock);
                document.body.append(block);

                !item.likeStatus ? document.querySelector('.like-current-block').classList.add('check') : document.querySelector('.like-current-block').classList.remove('check');
                !item.dislikeStatus ? document.querySelector('.dislike-current-block').classList.add('check') : document.querySelector('.dislike-current-block').classList.remove('check');
            }
        });
        event.stopPropagation()
    }, false);
}

// Закрытие при нажатии на элементы вне окна текущего элемента
document.addEventListener('click', function (event) {

    if (document.getElementsByClassName('current-img-block').length) {

        document.querySelector('.opacity-block').remove();
        document.forms[0][1].value = '';
        document.forms[0][2].value = '';
        document.getElementsByClassName('current-img-block')[0].remove();
        document.querySelector('main').removeAttribute('style');

        block.querySelector('.img-block img').remove();
        block.querySelector('.like-current-block p').innerText = '';
        block.querySelector('.dislike-current-block p').innerText = '';
        block.querySelector('.all-comments').innerHTML = '';
    }
}, false);

// ESC закрытие
document.addEventListener('keydown', function (event) {
    const key = event.key;
    if (key === "Escape") {

        if (document.getElementsByClassName('current-img-block').length) {

            document.querySelector('.opacity-block').remove();
            document.forms[0][1].value = '';
            document.forms[0][2].value = '';
            document.getElementsByClassName('current-img-block')[0].remove();
            document.querySelector('main').removeAttribute('style');

            block.querySelector('.img-block img').remove();
            block.querySelector('.like-current-block p').innerText = '';
            block.querySelector('.dislike-current-block p').innerText = '';
            block.querySelector('.all-comments').innerHTML = '';

        }
    }
});